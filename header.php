<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />

  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <meta name="description" content="" />
  <meta name="author" content="Jakob Pless" />

  <!-- [if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif] -->

  <link type="text/css" rel="stylesheet" href="http://fast.fonts.com/cssapi/d64d14ae-b2a5-425c-bf36-a81b9daa2efe.css" />
  <link type="text/css" rel="stylesheet" href="http://fast.fonts.com/cssapi/d64d14ae-b2a5-425c-bf36-a81b9daa2efe.css" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/stylesheets/styles.css" />
  
  <?php wp_head(); ?>
</head>
<body>
<img id="headerImg" src="<?php echo get_template_directory_uri(); ?>/images/sommer.png" />

<header class="container">
	<div class="row">
		<nav class="offset10 span2">
			<!--<ul class="inline pull-right">
				<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/dk.png" width="30" height="23" /></a></li>
				<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/uk.png" width="30" height="23" /></a></li>
			</ul>-->
            <?php dynamic_sidebar('language-sidebar'); ?>
		</nav>
	</div>

	<div class="row">
		<div id="logo" class="span12">
			<a href="<?php site_url(); ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" />
			</a>
		</div>
	</div>

	<div class="row">
		<div class="navbar span12">
			<?php wp_nav_menu( array(
				'theme_location'  => 'topnav',
				'container'       => 'nav', 
				'container_class' => 'navbar-inner',
				'menu_class'      => 'nav'
			) ); ?>
		</div>
	</div>

	<div class="row">
		<div class="span12">
			<?php if(function_exists('wpsagn_breadcrumbs')) wpsagn_breadcrumbs(); ?>
		</div>
	</div>
</header>