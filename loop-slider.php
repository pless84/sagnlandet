<div class="slider-wrapper">
	<div class="container">
		<div class="row">
			<div class="span8">
				<div id="featured" class="carousel slide">
					<div class="carousel-inner">
						<div class="item active"><img src="<?php echo get_template_directory_uri(); ?>/images/slider/front1.jpg" /></div>
						<div class="item"><img src="<?php echo get_template_directory_uri(); ?>/images/slider/front2.jpg" /></div>
						<div class="item"><img src="<?php echo get_template_directory_uri(); ?>/images/slider/front3.jpg" /></div>
					</div>
				</div>
			</div>
		
			<div class="span4">
				<div id="accordion" class="accordion">
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
								<h5>Oplevelser &amp; Aktiviteter</h5>
							</a>
						</div>
						<div id="collapseOne" class="accordion-body collapse in">
							<div class="accordion-inner">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus unde temporibus nam doloribus est sint quos laudantium dolores deleniti illum officiis non. Eaque totam debitis quod labore! Cumque quas quasi odit deserunt!</p>
								<p><a href="">Læs mere...</a></p>
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
								<h5>Undervisning</h5>
							</a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
							<div class="accordion-inner">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus unde temporibus nam doloribus est sint quos laudantium dolores deleniti illum officiis non. Eaque totam debitis quod labore! Cumque quas quasi odit deserunt!</p>
								<p><a href="">Læs mere...</a></p>
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
								<h5>Teambuilding</h5>
							</a>
						</div>
						<div id="collapseThree" class="accordion-body collapse">
							<div class="accordion-inner">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus unde temporibus nam doloribus est sint quos laudantium dolores deleniti illum officiis non. Eaque totam debitis quod labore! Cumque quas quasi odit deserunt!</p>
								<p><a href="">Læs mere...</a></p>
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
								<h5>Møder &amp; Konferencer</h5>
							</a>
						</div>
						<div id="collapseFour" class="accordion-body collapse">
							<div class="accordion-inner">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus unde temporibus nam doloribus est sint quos laudantium dolores deleniti illum officiis non. Eaque totam debitis quod labore! Cumque quas quasi odit deserunt!</p>
								<p><a href="">Læs mere...</a></p>
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
								<h5>Private Arrangementer</h5>
							</a>
						</div>
						<div id="collapseFive" class="accordion-body collapse">
							<div class="accordion-inner">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus unde temporibus nam doloribus est sint quos laudantium dolores deleniti illum officiis non. Eaque totam debitis quod labore! Cumque quas quasi odit deserunt!</p>
								<p><a href="">Læs mere...</a></p>
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
								<h5>Forskning &amp; Rekonstruktion</h5>
							</a>
						</div>
						<div id="collapseSix" class="accordion-body collapse">
							<div class="accordion-inner">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus unde temporibus nam doloribus est sint quos laudantium dolores deleniti illum officiis non. Eaque totam debitis quod labore! Cumque quas quasi odit deserunt!</p>
								<p><a href="">Læs mere...</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>