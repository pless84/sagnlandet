<div id="info-boxes" class="row">
	<?php query_posts( 'posts_per_page=3&category_name=infoboks' );
	if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<!-- post -->

	<div class="span4">
    	<a href="<?php the_permalink(); ?>">
			<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/billet.png" /> -->
			<?php the_post_thumbnail( 'infoboks' ); ?>
			<h5><?php the_title(); ?></h5>
        </a>
	</div>

	<?php endwhile; else: ?>
	<!-- no posts found -->
	<div class="span12">
		<h1>No Info Boxes</h1>
	</div>
	<?php endif; ?>
	<!-- <div class="span4">
		<img src="<?php echo get_template_directory_uri(); ?>/images/kort.png" />
		<h5>Kort over Sagnlandet</h5>
	</div>
	<div class="span4">
		<img src="<?php echo get_template_directory_uri(); ?>/images/moedos.png" />
		<h5>Mød os i Sagnlandet</h5>
	</div> -->
</div>