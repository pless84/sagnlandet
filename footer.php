<div class="footer-wrapper">
	<footer id="footer" class="container">
		<div class="row">
			<div class="span4">
				<ul>
					<?php wp_list_pages(array(
                        'depth'		=> 1,
                        'child_of'	=> 952,
                        'title_li'	=> '<h5>' . __('Praktisk Info') . '</h5>'
                    )); ?>
				</ul>
			</div>
			<div class="span4">
				<ul>
					<?php wp_list_pages(array(
                        'depth'		=> 1,
                        'child_of'	=> 953,
                        'title_li'	=> '<h5>' . __('Om Sagnlandet Lejre') . '</h5>'
                    )); ?>
				</ul>
			</div>
			<div class="span4">
				<ul>
					<?php wp_list_pages(array(
                        'depth'		=> 1,
                        'child_of'	=> 954,
                        'title_li'	=> '<h5>' . __('Danmark Vugge') . '</h5>'
                    )); ?>
				</ul>
			</div>
		</div>

		<div class="row">
			<div class="span12">
				<address class="text-center">Slangealleen 2, DK-4320 Lejre, Tlf.: +45 4648 0878, <a href="#">kontakt@sagnlandet.dk</a></address>
			</div>
		</div>
	</footer>
</div>

<script>
    jQuery('#featured').carousel();

 //    jQuery('#newsSlider').flexslider({
	// 	animation: "slide",
	// 	animationLoop: false,
	// 	itemWidth: 210,
	// 	itemMargin: 5,
	// 	minItems: 3,
	// 	maxItems: 3,
	// 	controlNav: true
	// });
</script>
<?php wp_footer(); ?>
</body>
</html>