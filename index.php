<?php get_header(); ?>

<?php get_template_part( 'loop', 'slider' ); ?>

<div class="container">
	<div id="info-boxes" class="row">
		<div class="span4">
			<img src="<?php echo get_template_directory_uri(); ?>/images/billet.png" />
			<h5>Billetten gælder hele sæsonen</h5>
		</div>
		<div class="span4">
			<img src="<?php echo get_template_directory_uri(); ?>/images/kort.png" />
			<h5>Kort over Sagnlandet</h5>
		</div>
		<div class="span4">
			<img src="<?php echo get_template_directory_uri(); ?>/images/moedos.png" />
			<h5>Mød os i Sagnlandet</h5>
		</div>
	</div>

	<div class="row">
		<div id="newsSlider" class="span12">
			<ul class="slides">
				<li><img src="http://placehold.it/600x800&text=FooBar1"></li>
				<li><img src="http://placehold.it/600x800&text=FooBar2"></li>
				<li><img src="http://placehold.it/600x800&text=FooBar3"></li>
				<li><img src="http://placehold.it/600x800&text=FooBar4"></li>
				<li><img src="http://placehold.it/600x800&text=FooBar5"></li>
				<li><img src="http://placehold.it/600x800&text=FooBar6"></li>
				<li><img src="http://placehold.it/600x800&text=FooBar7"></li>
				<li><img src="http://placehold.it/600x800&text=FooBar8"></li>
				<li><img src="http://placehold.it/600x800&text=FooBar9"></li>
			</ul>
		</div>
	</div>
</div>

<?php get_footer(); ?>