<?php 

function wpsagn_setup_theme() {

	if ( function_exists( 'register_sidebar' ) ) {
		register_sidebar( array(
			'name'	=> 'Sprog Vælger',
			'id' => 'language-sidebar',
			'description' => __( 'Widgets in this area will be shown on the right-hand side.' ),
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '<h3>',
			'after_title' => '</h3>'
		) );
	}

	register_nav_menus( array(
		'topnav' => 'Top Navigation',
	) );

	add_theme_support( 'post-thumbnails' );
	add_image_size( 'infoboks' );
}
add_action( 'after_setup_theme', 'wpsagn_setup_theme' );

function wpsagn_scripts_with_jquery() {
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/javascripts/bootstrap.js', array('jquery'), '20130310' );

	// flexSlider
	wp_enqueue_style( 'flexSlider-style', get_template_directory_uri() . '/stylesheets/flexslider.css' );
	wp_enqueue_script( 'flexSlider-script', get_template_directory_uri() . '/javascripts/jquery.flexslider-min.js', array('jquery'), '20130310' );
}
add_action( 'wp_enqueue_scripts', 'wpsagn_scripts_with_jquery' );

function wpsagn_breadcrumbs() {
	$showOnHome = 1; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$delimiter = '&raquo;'; // delimiter between crumbs
	$home = 'Forside'; // text for the 'Home' link
	$showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
	$before = '<span class="current">'; // tag before the current crumb
	$after = '</span>'; // tag after the current crumb
	global $post;
	$homeLink = get_bloginfo('url');
	if (is_home() || is_front_page()){
		if ($showOnHome == 1) echo '<div class="breadcrumb">Du er her: <a href="' . $homeLink . '">' . $home . '</a></div>';
	} else {
		echo '<div class="breadcrumb">Du er her: <a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
		if (is_category()){
			$thisCat = get_category(get_query_var('cat'), false);
			if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
			echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;
		} elseif (is_search()){
			echo $before . 'Search results for "' . get_search_query() . '"' . $after;
		} elseif (is_day()){
			echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
			echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
			echo $before . get_the_time('d') . $after;
		} elseif(is_month()){
			echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
			echo $before . get_the_time('F') . $after;
		} elseif(is_year()){
			echo $before . get_the_time('Y') . $after;
		} elseif (is_single() && !is_attachment()){
			if (get_post_type() != 'post'){
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
				if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
				if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
				echo $cats;
				if ($showCurrent == 1) echo $before . get_the_title() . $after;
			}
		} elseif(!is_single() && !is_page() && get_post_type() != 'post' && !is_404()){
			$post_type = get_post_type_object(get_post_type());
			echo $before . $post_type->labels->singular_name . $after;
		} elseif (is_attachment()){
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
			echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
			if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
		} elseif (is_page() && !$post->post_parent){
			if ($showCurrent == 1) echo $before . get_the_title() . $after;
		} elseif (is_page() && $post->post_parent){
			$parent_id = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
				$parent_id = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			for ($i = 0; $i < count($breadcrumbs); $i++) {
				echo $breadcrumbs[$i];
				if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
			}
			if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
		} elseif (is_tag()){
			echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
		} elseif (is_author()){
			global $author;
			$userdata = get_userdata($author);
			echo $before . 'Articles posted by ' . $userdata->display_name . $after;
		} elseif (is_404()){
			echo $before . 'Error 404' . $after;
		}
		if (get_query_var('paged')){
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
				echo __('Page') . ' ' . get_query_var('paged');
				if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
	}
	echo '</div>';
	}
}

function wpsagn_slider_post() {
   /**
    * Register a custom post type
    *
    * Supplied is a "reasonable" list of defaults
    * @see register_post_type for full list of options for register_post_type
    * @see add_post_type_support for full descriptions of 'supports' options
    * @see get_post_type_capabilities for full list of available fine grained capabilities that are supported
    */
    register_post_type( 'slides', array(
        'public' => true,
        'has_archive' => true,
        'labels' => array(
            'name' => __( 'Slides', 'wpsagn' ),
            'singular_name' => __( 'Slide', 'wpsagn' )
        ),
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions', 'page-attributes', 'custom-fields' )
    ) );
}
add_action( 'init', 'wpsagn_slider_post');


?>